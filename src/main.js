import Vue from 'vue'
import App from './App.vue'
import router from './plugins/router'
import store from './plugins/store'
import Vuetify from 'vuetify'
import vuetify from './plugins/vuetify'

Vue.use(Vuetify);

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,

  // vuetify,
  render: function (h) { return h(App) }
}).$mount('#app')
